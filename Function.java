package compiler;

public class Function{
	String fname;
	String parameter;
	String paramType;
	Object paramValue;
	
	public Function(String name, String parameter){
		this.parameter = parameter;
		this.fname = name;
		paramType = "";
	}	
	public String getParam(){
			return parameter;
	}
	
	public void setParamType(String s){
			paramType = s;
	}
	
	public void setParamValue(String s){
			paramValue = s;
	}
	
	public String getFName(){
		return fname;
	}
	
}
