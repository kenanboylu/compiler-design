package compiler;

public class Variable{
	
	String identifer;
	Object value;
	
	public Variable(String identifer){
		this.identifer = identifer;	
	}
	
	public String getIdentifier(){
		return identifer;
	}
	
	public void setValue(Object o){
		value = o;	
	}
	
	public Object getValue(){
		return value;
	}
}
