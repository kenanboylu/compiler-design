package compiler;

//Kenan Boylu && Mustafa Akgilli Proje

import java.io.*;
import java.util.LinkedList;
import java.lang.Character;
import java.util.ArrayList;
import java.util.HashMap;

public class Parser {

	// /static HashMap<String, Function> Functmap = new HashMap<String,
	// Function>();
	static HashMap<String, Variable> hm = new HashMap<String, Variable>(); //Simple Table
	static ArrayList<Function> funcs = new ArrayList<Function>();
	static ArrayList<String> stack = new ArrayList<String>();              //fuction variavle

	static String token, lin, output, outprint = "",
			faultprint = "Error! No entry", str;
	static LinkedList<String> tokenlist, list;
	static LinkedList<String> line;
	static String[] keys = { "class", "string", "int", "char", "double",
			"boolean", "void", "if", "while", "do", "for" };
	static int checkclassname = 0;
	static int n = 0;
	static String functname;
	static String functparam = "";
	static String functback;
	static Function f;

	public Parser(File f) {

		try {
			FileInputStream is = new FileInputStream(f);

			readFile(is); // read File

			program();
			//
			if (output == null)
				System.out.println("Your code is compiled successfuly.");
			else {
				output = output.replace("null", "");
				System.out.println(output);
			}

		} catch (IOException ioe) {
			System.out
					.print("File is not found. Please check path of input file.");
			System.exit(0);
		}

	}

	public static String getOut() {
		return output;
	}

	public static String poll(LinkedList<String> list) {
		if (list.peek() == null) {
			System.out.println("Error: end of class; '}' is expected...");
			System.exit(0);
		}
		return list.poll();
	}

	public static String pollLast(LinkedList<String> list) {
		if (list.peek() == null) {
			System.out.println("Error: end of class; '}' is expected...");
			System.exit(0);
		}
		return list.pollLast();
	}

	public static void readFile(FileInputStream is) {
		try {
			Reader r = new BufferedReader(new InputStreamReader(is));
			StreamTokenizer tokenizer = new StreamTokenizer(r);
			list = new LinkedList<String>();
			line = new LinkedList<String>();
			boolean b = false;
			tokenizer.ordinaryChar(34);
			tokenizer.ordinaryChar(39);
			tokenizer.ordinaryChar('-');
			tokenizer.ordinaryChar('/');

			while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
				if (tokenizer.ttype == StreamTokenizer.TT_WORD) {
					if (b) {
						str = pollLast(list);
						pollLast(line);
						str = str.concat(tokenizer.sval);
						list.add(str);
						line.add(Integer.toString(tokenizer.lineno()));
						b = false;
					} else {
						list.add(tokenizer.sval);
						line.add(Integer.toString(tokenizer.lineno()));
					}
				} else if (tokenizer.ttype == StreamTokenizer.TT_NUMBER) {
					if (b) {
						str = pollLast(list);
						pollLast(line);
						str = str.concat(Double.toString((int) tokenizer.nval));
						list.add(str);
						line.add(Integer.toString(tokenizer.lineno()));
						b = false;
					} else {
						list.add(Double.toString(tokenizer.nval));
						line.add(Integer.toString(tokenizer.lineno()));
					}
				} else if (tokenizer.ttype == StreamTokenizer.TT_EOL) {
					list.add(System.getProperty("line.separator"));
					line.add(Integer.toString(tokenizer.lineno()));
				} else {
					String s = tokenizer.toString().substring(7, 8);
					if (s.equals("[") || s.equals("]") || s.equals("{")
							|| s.equals("}") || s.equals(";") || s.equals(",")
							|| s.equals("+") || s.equals("-") || s.equals("*")
							|| s.equals("/") || s.equals("|") || s.equals("&")
							|| s.equals("=") || s.equals("^") || s.equals("%")
							|| s.equals("<") || s.equals(">") || s.equals("(")
							|| s.equals(")") || s.equals(":") || s.equals("\"")
							|| s.equals("\'") || s.equals("!")) {
						list.add(s);
						line.add(Integer.toString(tokenizer.lineno()));
						b = false;
					} else {
						str = pollLast(list);
						pollLast(line);
						str = str.concat(s);
						list.add(str);
						line.add(Integer.toString(tokenizer.lineno()));
						b = true;
					}
				}
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.exit(0);
		}
	}

	public static boolean contain(String s) {
		int size = keys.length;
		for (int i = 0; i < size; i++) {
			if (keys[i].equals(s))
				return true;
		}
		return false;
	}

	public static void program() {
		str = poll(list);
		lin = poll(line);

		if (!str.equals("class"))
			output += "Error ! Line:1  'class' keyword  expected...Program should start class keyword \n";

		str = poll(list);
		lin = poll(line);

		if (!Character.isLetter(str.charAt(0))) {
			output += "Error ! Line:1   class name does not start with a number.\n";
			checkclassname = 1;
		}

		str = poll(list);
		lin = poll(line);
		while (lin.equals("1") && !str.equals("{")) {
			if (checkclassname == 0) {
				checkclassname = 1;
				output += "Error ! Line:1 class name is wrong.\n";
			}
			str = poll(list);
			lin = poll(line);
		}

		if (lin.equals("2")) {
			output += "Error ! Line:1 '{' is expected\n";
		} else {
			str = poll(list);
			lin = poll(line);
			if (lin.equals("1")) {
				output += "Error ! Line: 1 Remove characters after { \n";
			}
			while (lin.equals("1")) {
				str = poll(list);
				lin = poll(line);
			}
		}
		int un = 0;
		// System.out.println(str);
		while (!str.equals("function")) {
			// System.out.println(str);
			start();

		}
		// str = poll(list);
		// lin = poll(line);

		while (!str.equals("}") && str.equals("function")) {
			// System.out.println("before func===>"+str);
			function();
		}
		
		while (!str.equals("}") && str.equals("main")) {
			// System.out.println("before func===>"+str);
			main();
		}
		if (!str.equals("}"))
			output += "Error ! Line:" + lin + " }  expected\n";
		//str = poll(list);
		//lin = poll(line);
	}

	public static void start() {
		String temp = str;
		// System.out.println(str);

		boolean b = identifier();
		while (b || !str.equals("function")) {

			if (str.equals("}"))
				return;
			else if (b) {
				varDescription(temp, 0);

			} 

			else
			str = poll(list);
			lin = poll(line);
			// System.out.println("BURSI DA NE..."+str);
			temp = str;
			b = identifier();

		}

	}

	public static void varDescription(String s, int op) {
		Variable d;
		
		// System.out.println("VatDes==>"+str);
		String tempstr = str;
		if (str.equals(";")) {
			output += "Error ! Line:" + lin + " variable name is invalid\n";
			str = poll(list);
			lin = poll(line);
			return;
		}
		str = poll(list);
		lin = poll(line);
		
		 //System.out.println("Eh"+str);
		if (!str.equals(";") && !str.equals("=")) {
			output += "Error ! Line:" + lin + " variable name is invalid\n";
			endOfLine();
			return;
		}
		//System.out.println("dd" + tempstr);
		String temp = tempstr.concat(Integer.toString(n+1));
		//System.out.println("dd" + temp);
		d = new Variable(s);
		hm.put(temp, d);
        if(n==1)
        	stack.add(temp);
        
        
		if (str.equals(";")) {
			return;

		}

		else if (str.equals("=")) {
			str = poll(list);
			lin = poll(line);
			if(!str.equals("\""));
			d.setValue(str);
			if (str.equals(";")) {
				output += "Error ! Line:" + lin + "   a value expected\n";
				str = poll(list);
				lin = poll(line);
				return;
			} else {
				//System.out.println("STRR===>" + str);
				if(str.equals("\"") || str.equals("\'")){
					str = poll(list);
					lin = poll(line);
					d.setValue(str);
					//System.out.println("hi===>" + str);
					str = poll(list);
					lin = poll(line);
				}
				str = poll(list);
				lin = poll(line);
				if (!str.equals(";")) {
					int foo = Integer.parseInt(lin);
					output += "Error ! Line:" + (foo-1) + "    ;  expected\n";
					return;
				} else {
					str = poll(list);
					lin = poll(line);
				}
			}

		}

	}

	public static void function() {
	 //System.out.println("func==>"+str);
		if (!str.equals("function")) {
			if (str.equals("}"))
				return;
			output += "Error ! Line:" + lin + "    blocks is empty...  \n";
		} else {
			n++;
			str = poll(list);
			lin = poll(line);
		}

		String s;

		//System.out.println("Func name >"+str);
		functname = str;
		//System.out.println("Func name1 >"+str);
		str = poll(list);
		lin = poll(line);
		while (!str.equals("(")) {
			output += "Error ! Line:" + lin + " function name is wrong... \n";
			str = poll(list);
			lin = poll(line);
			//System.out.println("Func name3 >"+str);
		}
		n=1;
		argumanlist();

		str = poll(list);
		lin = poll(line);
		//System.out.println("Func name2 >"+str);
		n=1;
		blok();
       
		Function f = new Function(functname, functparam);
		// System.out.println("Func name GGG>"+f);
		funcs.add(f);
		
		for(int i=0; i<stack.size(); i++){
			//System.out.println("silinen========= " +stack.get(i));
			hm.remove(stack.get(i));
		}
		//Variable v=new Variable(functparam);
		//v.getIdentifier();
		//System.out.println("Func " +v.getIdentifier());
		//Variable v = hm.get("num1");
		//System.out.println("v---->" + v.getIdentifier());
	    //System.out.println("Func name GGG>"+functparam);

		/*
		 * str = poll(list); lin = poll(line);
		 * 
		 * 
		 * s = str.concat(Integer.toString(n)); variable(s,0);
		 * 
		 * str = poll(list); lin = poll(line);
		 */

	}

	public static void argumanlist() {
		String backup;
		
		// System.out.println("arguman begin==>"+str);
		if (!str.equals("("))
			output += "Error ! Line:" + lin + " ( expected\n";
		else {
			str = poll(list);
			lin = poll(line);
			// System.out.println("arguman type==>"+str);
		}

		if (str.equals(")")) {
			str = poll(list);
			lin = poll(line);
			// System.out.println("arguman1==>"+str);
			return;
		}
		boolean b;
		if (!str.equals(")") && !str.equals("{")) {
			b = identifier();
			String tempstr = str;
			if (b) {
				 //System.out.println("parameter==>"+tempstr);
				functparam = tempstr;

			} else {
				output += "Error ! Line:" + lin + " '" + str
						+ "' variable is undefined\n";
				str = poll(list);
				lin = poll(line);

			}
			if (!str.equals(")") && !str.equals("{")) {
				b = contain(str);
				// System.out.println("Tempstr==>"+tempstr);
				if (!b && tempstr.equals(functparam)) {
					String var = variable(tempstr, 0);
					if (var.equals("hata")) {
						Variable d = new Variable(tempstr);
						String temp = str.concat(Integer.toString(n+1));
						 //System.out.println("num==>"+d);
						 //System.out.println("num==>"+temp);
						hm.put(temp, d);
						d.setValue(null);
						stack.add(temp);
						//hm.remove(temp);
						/*Variable r = hm.get(temp);
						System.out.println("v---->" + r);
						System.out.println("v---->" + r.getValue());**/
						// System.out.println(hm.get(str));
					}
				} else
					// isim(1);
					str = poll(list);
				lin = poll(line);
				while (!str.equals(")") && !str.equals("{")) {
					str = poll(list);
					lin = poll(line);

				}
			}
		}
		if (str.equals("{")) {
			output += "Error ! Line:" + lin + " ) value is expected\n";
			return;
		}
		// str = poll(list);
		// lin = poll(line);
	}

	public static void blok() {
		// System.out.println("blok==>"+str);
		int u = 0;
		if (!str.equals("{"))
			output += "Error ! Line:" + lin + " '{' is expected.\n";
		else {
			str = poll(list);
			// lin = poll(line);

		}
		if (str.equals("}"))
			output += "Error ! Line:" + lin + "   blocks is empty...  \n";

		while (!str.equals("}")) {
			statement();
			u++;
		}
		if (u == 1)
			output += "Error ! Line:" + lin + " only unique variable...\n";

		str = poll(list);
		lin = poll(line);

	}

	public static void statement() {
		 //System.out.println("statement==>"+str);
		if (str.equals("while"))
			loop();

		else if (str.equals("if") || str.equals("elif"))
			selection();

		else{
			//System.out.println("statement==>"+str);
			komut();
			
		}
			
			

	}

	public static void selection() {
		str = poll(list);
		// lin = poll(line);
		// System.out.println("selection==>"+str);
		if (!str.equals("("))
			output += "Error ! Line:" + lin + " ( is expected\n";
		else {
			str = poll(list);
			// lin = poll(line);
			 //System.out.println("selection==>"+str);
		}

		control();

		if (!str.equals(")"))
			output += "Error ! Line:" + lin + " ) is expected\n";
		else {
			str = poll(list);
			lin = poll(line);
		}
		blok();

		String next = list.peekFirst();
		if (next.equals("if")) {
			str = poll(list);
			lin = poll(line);
			selection();
		}
	}

	public static void control() {
		 //System.out.println("selection==>"+str);
		try {
			double d = Double.parseDouble(str);

		} catch (NumberFormatException nfe) {
			String s = str.concat(Integer.toString(1));
			String dgskn = variable(s, 1);
			if (dgskn.equals("hata")) {
				output += "Error ! Line:" + lin +" "+ str +" is undefined.\n";
			}
			if (isNUll(s).equals("nn"))
				output += "Error ! Line:" + lin + "   " + str
						+ " is Null variable.\n";
		}
		str = poll(list);
		lin = poll(line);

		String next;
		next = list.peekFirst();
		if ((str.equals("<") && next.equals("="))
				|| (str.equals("=") && next.equals("="))
				|| (str.equals(">") && next.equals("="))
				|| (str.equals("!") && next.equals("="))) {
			str = poll(list);
			lin = poll(line);
			str = poll(list);
			lin = poll(line);
			try {
				double d = Double.parseDouble(str);

			} catch (NumberFormatException nfe) {
				String s = str.concat(Integer.toString(n));
				variable(s, 0);
			}
			str = poll(list);
			lin = poll(line);
		} else if (str.equals("<") || str.equals(">")) {
			str = poll(list);
			lin = poll(line);
			try {
				double d = Double.parseDouble(str);

			} catch (NumberFormatException nfe) {
				String s = str.concat(Integer.toString(n));
				variable(s, 0);

			}

			str = poll(list);
			lin = poll(line);
		}
	}

	public static void loop() {
		str = poll(list);
		// lin = poll(line);
		// test = false;
		// System.out.println("loop==>"+str);
		if (!str.equals("("))
			output += "Error ! Line:" + lin + " ( is expected\n";
		else {
			str = poll(list);
			lin = poll(line);
		}
		// System.out.println("loop2==>"+str);
		control();

		if (!str.equals(")"))
			output += "Error ! Line:" + lin + " ( is expected\n";
		else {
			str = poll(list);
			lin = poll(line);
		}
		blok();

		String next = list.peekFirst();
		if (next.equals("loop")) {
			str = poll(list);
			lin = poll(line);
			control();
		}
	}

	public static void komut() {
		 //System.out.println("Gelen========>"+str+lin);
		int num1 = 0;
		String temp = str;
		//System.out.println("Bu daaaaaaaaaa"+temp);
		boolean b = identifier();
		if (b || !str.equals("operation")) {
			 //System.out.println("komut112==>"+str);
			 if (str.equals("printf"))
					printMethod();
			        
			 else
			    varDescription(temp, 0);

		}

		else if (str.equals("operation"))
			aritmetik();
		str = poll(list);
		lin = poll(line);
		return;
		// else
		// atama(1);
	}

	// }
	public static void printMethod(){
		//System.out.println("komut114==>"+str);
		str = poll(list);
		lin = poll(line);
		if(!str.equals("(")){
			output += "Error ! Line:" + lin + " ( expected\n";
		}
		else{
			str = poll(list);
			lin = poll(line);
			 //System.out.println("y------------------>"+str);
			if(str.equals("\"")){
				str = poll(list);
			    lin = poll(line);
			    
				while(!str.equals("\"")){
					//System.out.println("komut118==>"+str);
				str = poll(list);
			    lin = poll(line);
			}
				//System.out.println("komut118==>"+str);
			
				if(!str.equals("\"")){
					output += "Error ! Line:" + lin + " \" expected\n";
				}
				 str = poll(list);
			     lin = poll(line);
			    
			     if(!str.equals(")"))
			    	 output += "Error ! Line:" + lin + "   ) is expected\n";
			     str = poll(list);
			     lin = poll(line);
			     if(!str.equals(";"))
			    	 output += "Error ! Line:" + lin + "   ; is expected\n";
			
			}
			else{
				//System.out.println("y------------------->"+str);
				String s = str.concat(Integer.toString(2));
				//System.out.println("y------------------->"+s);
				String var = variable(s, 0);
				if (var.equals("hata")) 
					output += "Error ! Line:" + lin + "   " + str + " is undefined variable.\n";
				
				str = poll(list);
			    lin = poll(line);
			    
			     if(!str.equals(")"))
			    	 output += "Error ! Line:" + lin + "   ) is expected\n";
			     else{
			     str = poll(list);
			     lin = poll(line);
			     }
			     if(!str.equals(";"))
			    	 output += "Error ! Line:" + lin + "   ; is expected\n";
			}
		}
	
		
		
	}
	public static void aritmetik() {
        int t=0;
		String next;
		String val;
		// str = poll(list);
		// lin = poll(line);
		 //System.out.println("aritmetic=="+str);
		while (!str.equals(";")) {
			next = list.peekFirst();
			// System.out.println("next=="+next);
			 
			if (str.equals("-") || str.equals("+") || str.equals("/")
					|| str.equals("*")) {
				if (next.equals("+") || next.equals("/") || next.equals("*")
						|| next.equals("-") || next.equals(";")) {
					output += "Error ! Line:" + lin + " a number is expected\n";
					str = poll(list);
					lin = poll(line);
					str = poll(list);
					lin = poll(line);
				} else {
					str = poll(list);
					lin = poll(line);
				    //System.out.println("Aritmetic=="+str);

				}
				
			}

			else {

				str = poll(list);
				lin = poll(line);
				// System.out.println("son=="+str);
			}
			t=t+1;
			//System.out.println("str=="+str);
			//System.out.println("NNNNNNNNN=="+(n+1));
			String s1 = str.concat(Integer.toString(1));
			String s2 = str.concat(Integer.toString(2));
			String var = variable(s1,1);
			String var2 = variable(s2,1);
			//System.out.println("var=="+var);
			if(!isNumeric(str))
			if(!str.equals("int") && !str.equals(";") && !str.equals("=") && 
					!str.equals("/") && !str.equals("+") && !str.equals("-") && !str.equals("*"))
			if (var.equals("hata") && var2.equals("hata")) 
				output += "Error ! Line:" + lin + "   " + str + " is undefined variable.\n";	
		}
	}
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	public static Function getFunction(String fname) {
		// System.out.println(fname);
		for (Function f : funcs) {
			// System.out.println(f.getFName());
			if (f.getFName().equals(fname)) {
				return f;
			}
		}
		return null;
	}

	public static String callFunction() {

		String temp = "";
		str = poll(list);
		lin = poll(line);
		// System.out.println("scall"+str);
		f = getFunction(str);

		if (f == null) {
			output += "Error ! Line:" + lin + "  Function " + str
					+ " is not found.\n";
			temp = "hata";
			while (!str.equals(")") && !str.equals(";")) {
				str = poll(list);
				lin = poll(line);
			}
			if (str.equals(")")) {
				str = poll(list);
				lin = poll(line);
			}
		} else {
			str = poll(list);
			lin = poll(line);
			if (str.equals("(")) {
				str = poll(list);
				lin = poll(line);
			}

		}

		return temp;

	}
	
	public static void main() {
		
		str = poll(list);
		lin = poll(line);
		str = poll(list);
		lin = poll(line);
		str = poll(list);
		lin = poll(line);
		
		
		str = poll(list);
		lin = poll(line);
		
		while(!str.equals("}")){
		if (str.equals("call")){
			callFunction();
		}
		else if (str.equals("int") || str.equals("double") || str.equals("printf") || str.equals("string")){ 
			komut();
			}
		
		str = poll(list);
		lin = poll(line);
		//System.out.println("s------------------------------------>" + str);
		}
	}

	public static String variable(String s, int check) {
		if (!hm.containsKey(s)) {
			//output += "Error ! Line:"+lin+" "+s+" variable  is undefined \n";
			return "hata";
		}
		//System.out.println("s---->" + str);
		Variable d = hm.get(s);
		//System.out.println("v---->" + d.getValue());
		//System.out.println("s---->" + d.getIdentifier());
		return d.getIdentifier();
	}

	public static String isNUll(String s) {
		String ss=null;
		Variable d = hm.get(s);
		 ss = (String) d.getValue();
		//System.out.println("isnull==>>"+ss);
		if (ss == null)
			return "nn";

		return "ff";
	}

	public static void endOfLine() {
		String x = lin;
		str = poll(list);
		lin = poll(line);
		System.out.println(str + " " + x + "" + lin);
		while (x.equals(lin)) {
			x = lin;
			str = poll(list);
			lin = poll(line);
		}
	}

	public static boolean identifier() {
		if (str.equals("int") || str.equals("double") || str.equals("string")
				|| str.equals("char") || str.equals("boolean")) {
			str = poll(list);
			lin = poll(line);
			return true;
		}
		return false;
	}

	public static void main(String[] args) throws IOException {

		File file = new File(
				"C:\\Users\\Kenan\\workspace\\proje\\src\\compiler\\source.txt");
		Parser s = new Parser(file);

	}

}
