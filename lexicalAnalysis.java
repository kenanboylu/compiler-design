package compiler;
//Kenan Boylu && Mustafa Akgilli


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.LinkedList;

public class lexicalAnalysis {
	
	static String token, line, outprint = "", faultprint = "Error! No entry", str;
	static LinkedList<String> tokenlist;
	static LinkedList<String> linelist;
	static String[] keys = {"class", "string","int","char", "double", "boolean", "void", "if","while","do","for"};
	
	public lexicalAnalysis(File f){
		try{
			FileInputStream file = new FileInputStream(f); 
			readFile(file);
			int n=0;
			while(tokenlist.peekFirst() != null){
				token = poll(tokenlist);
				line = poll(linelist);
				str = "";
				faultprint = "";
				
				
				if(token.equals("\"") && tokenlist.peekFirst() != null){
					string();
				}
				
				else if(token.equals("\'") && tokenlist.peekFirst() != null){
					Char();
				}
				else if(token.equals("(") && tokenlist.peekFirst() != null){
					outprint += "<brackets, " + token + ">";
				}
				else if(token.equals(")") && tokenlist.peekFirst() != null){
					outprint += "<brackets, " + token + ">";
				}else if(token.equals("{") && tokenlist.peekFirst() != null){
					outprint += "<brackets, " + token + ">";
				}
				else if(token.equals("}") && tokenlist.peekFirst() != null){
					outprint += "<brackets, " + token + ">";
				}
			
				else{
					if(contain(token)){
						outprint += "\n<key, " + token + ">";
					}/* assign operator **/
					else if(token.equals("=")){
						outprint += "<assign_op, " + token + ">";
					} //logical operator
					else if(token.equals("|") && tokenlist.peekFirst().equals("|") ){
						outprint += "<logical_op, " + token + tokenlist.peekFirst() +">";
					}
					else if(token.equals("&") && tokenlist.peekFirst().equals("&") ){
						outprint += "<logical_op, " + token + tokenlist.peekFirst()+ ">";
					}// conditional operator
					else if((token.equals(">") && tokenlist.peekFirst().equals("="))
							 || (token.equals("<") && tokenlist.peekFirst().equals("=")) 
							 || (token.equals("!") && tokenlist.peekFirst().equals("="))){
						outprint += "<operator, " + token + tokenlist.peekFirst() + ">";
						token = poll(tokenlist);
						line = poll(linelist);
					} // aritmetik operations
					else if( token.equals("/") && tokenlist.peekFirst().equals("/")){
						continue;
					}
					else if(token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/")
							 || token.equals("<") || token.equals(">") ){
						outprint += "<operator, " + token + ">";
					}
					else if( token.equals(";") || token.equals("\'")){
						continue;
					}
					
					else {
						try{
							Double.parseDouble(token);
							outprint += "<number, " + token + ">";
						}
						catch(Exception e){
							
							if(token.equals("[") || token.equals("]") || token.equals(",") || token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/") ||
								token.equals("=") || token.equals("^") || token.equals("%") || token.equals("<") || token.equals(">") ||  token.equals(":") ||
								token.equals("\"") || token.equals("!")){
								faultprint = "Error! Entry: " + token +" Line: " + line;
								System.out.println(faultprint);
							
								continue;
							}
							else if(!token.equals("|") && !token.equals("&"))
								outprint += "<id, " + token + ">";
								
							
							
						}
					}
					
				}
				
				  
			}
			System.out.println(outprint);


		}catch(IOException ioe){
			ioe.printStackTrace();
			System.exit(0);
		}
	}
	
	public static void readFile(FileInputStream is){		
		try{
			Reader r = new BufferedReader(new InputStreamReader(is));
			StreamTokenizer tokenizer = new StreamTokenizer(r); 
			tokenlist = new LinkedList<String>(); 
			linelist = new LinkedList<String>(); 
			boolean b = false;
			tokenizer.ordinaryChar(34);
			tokenizer.ordinaryChar(39);
			tokenizer.ordinaryChar('-');
			tokenizer.ordinaryChar('/');
		
			while(tokenizer.nextToken() != StreamTokenizer.TT_EOF){
				if(tokenizer.ttype == StreamTokenizer.TT_WORD) {
					if(b){
						token = pollLast(tokenlist);
						pollLast(linelist);
						token = token.concat(tokenizer.sval);
						tokenlist.add(token);
						linelist.add(Integer.toString(tokenizer.lineno()));
						b = false;
						
						
					}
					else{
				  		tokenlist.add(tokenizer.sval);
				  		linelist.add(Integer.toString(tokenizer.lineno()));
				  		
				  	}
					
				}else if(tokenizer.ttype == StreamTokenizer.TT_NUMBER) {
					if(b){
						token = pollLast(tokenlist);
						pollLast(linelist);
						token = token.concat(Double.toString((int)tokenizer.nval));
						tokenlist.add(token);
						linelist.add(Integer.toString(tokenizer.lineno()));
						b = false;
						
					}
					else{
						tokenlist.add(Double.toString(tokenizer.nval));
						linelist.add(Integer.toString(tokenizer.lineno()));
						
					}
				}else if(tokenizer.ttype == StreamTokenizer.TT_EOL) {
					tokenlist.add(System.getProperty("line.separator"));
					linelist.add(Integer.toString(tokenizer.lineno()));
					
				} 
			    else{
					String s = tokenizer.toString().substring(7, 8);
					if(s.equals("[") || s.equals("]") || s.equals("{") || s.equals("}") || s.equals(";") || s.equals(",") || s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/") ||
						s.equals("|") || s.equals("&") || s.equals("=") || s.equals("^") || s.equals("%") || s.equals("<") || s.equals(">") || s.equals("(") || s.equals(")") || s.equals(":") ||
						s.equals("\"") || s.equals("\'") || s.equals("!")){
						tokenlist.add(s);
						linelist.add(Integer.toString(tokenizer.lineno()));
						b = false;
					}
					else{
						token = pollLast(tokenlist);
						pollLast(linelist);
						token = token.concat(s);
						tokenlist.add(token);
						linelist.add(Integer.toString(tokenizer.lineno()));
						b = true;
					}
				}
			}
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.exit(0);
		}
	}
	
	public static String poll(LinkedList<String> list){

		return list.poll();
	}
	
	public static String pollLast(LinkedList<String> list){
		return list.pollLast();
	}
	
	public static boolean contain(String s){
		int size = keys.length;
		for(int i = 0; i<size; i++){
			if (keys[i].equals(s))
				return true;
		}
		return false;
	}
	public static void Char(){
		String l;
		str += token;
		token = poll(tokenlist);
		line = poll(linelist);
		str += token;
		l = line;
		while(!token.equals("\'") && tokenlist.peekFirst() != null){
			token = poll(tokenlist);
			line = poll(linelist);
			str += token;
			
		}
		if(token.equals("\'")){
			outprint += "<char, " + str + ">";
		}
		else{
			faultprint = "Error! Entry: " + token +" Line: " + line;
			System.out.println(faultprint);
			System.exit(0);
		}
	
	}
	public static void string(){
		String l;
		str += token;
		
		token = poll(tokenlist);
		line = poll(linelist);
		str += token;
		
		l = line;
		while(!token.equals("\"") && tokenlist.peekFirst() != null){
			token = poll(tokenlist);
			line = poll(linelist);
			str += token;
			
		}
		if(token.equals("\"")){
			outprint += "<string, " + str + ">";
		}
		else{
			faultprint = "Error! Entry: " + token +" Line: " + line;
			System.out.println(faultprint);
			System.exit(0);
		}
		
	}
	public static void main(String[] args) {
		File file = new File("C:\\Users\\Kenan\\workspace\\proje\\src\\compiler\\source.txt");
		lexicalAnalysis lexi = new lexicalAnalysis(file);

	}

}
